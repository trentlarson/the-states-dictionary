# The State's Dictionary

### Contempt of Cop
n. Outrage against officers which can only be dealt with by enforcing some other written law.  (Outrage between state officials is a time-honored tactic to expand one's domain; see "Jurisdiction".)

### Fake News
n. Unapproved messages claiming to be truth, or true messages from unlicensed "journalists".

### Jurisdiction
n. The gerrymandered roles negotiated between different layers of our offices.  Unlike zero-sum games, the spoils are almost infinite.

### Money Laundering
n. Using money for things not visible to bureaucrats, much less assessable to tax collectors.

### Protest
n. Coordinated citizen complaining about this or that pet issue by the organizers.  Fortunately, because their aim has to be directed widely to attact one's fellows, the messages can often be diverted by well-placed agents so as to put pressure on spineless allies.

### Spirit
n. The biggest perpetually looming threat to state progress, but thankfully on the wane at the time of publication.
