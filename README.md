# The Statesman's Dictionary

In the spirit of [The Devil's Dictionary](TheDevilsDictionary.com), we offer the definitions of terms used by proponents of The State. We find these useful due to the wildly divergent use of English by professional politicians (eg the "[Patriot Act](https://www.eff.org/issues/patriot-act)").

All submissions welcome, including edits.  Thoughtful submissions respected.  Quality submissions (which are accurate, concise, humorous, eye-opening, and thought-provoking) are merged to master.
